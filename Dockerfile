FROM node:16 AS builder
WORKDIR '/app'

COPY package.json .
COPY yarn.lock .
RUN yarn install
COPY . .
RUN yarn build
RUN ls

FROM nginx as production
COPY ./default.conf /etc/nginx/conf.d/default.conf
COPY --from=builder /app/dist /usr/share/nginx/html