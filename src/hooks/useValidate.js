import { inject } from 'vue';

export default () => {
  const validFn = inject('validateFields', () => {});
  return {
    validFn,
  };
  // let validFn = getCurrentInstance().parent.ctx.validateFields;

  // console.log('validateFields', validateFields);

  // if (validateFields) {
  //   return;
  // }

  // // 看有沒有更好的做法 ?
  // // if (getCurrentInstance().parent.ctx.$options.name === 'ReFormItem') {
  // // console.log('getCurrentInstance()', getCurrentInstance());
  // if (getCurrentInstance().parent?.ctx?.$options?.name === 'ReFormItem') {
  //   validFn = getCurrentInstance().parent.ctx.validateFields;
  // }
  // else {
  //   validFn = () => {};
  // }

  // return {
  //   validFn,
  // };
};
